'use strict';

/* Теоретичні питання

 1. Оператор - це символ чи ключове слово, яке вказує виконання певної операції між одним чи декількома операндами. У виразі `a + b`, `+` - це оператор, що вказує на виконання додавання між `a` та `b`.
 Є оператори присвоєння "="; арифметичні оператори: "+" - додавання, "-" - віднімання, "*" - множення, "/" - ділення, "%" - залишок ділення;
 Оператори ще бувають унарні та бінарні. Унарний оператор працює з одним операндом (н.п.: -х, +х, ++х, --x...). Бінарний оператор працює з двома операндами (н.п: a + b, a == b, ...,
    Логічні оператори: ! (Логічне НЕ), || (Логічне АБО), && (Логічне І)). 

 2. Оператор порівняння порівнюють два значення, він повертає boolean знвчення вказуючи чи порівняння є true / false.
    let a = 5,
        b = 10;
        console.log(a == b); //false
        console.log(a >= b); //false
        console.log(a <= b); //true
        console.log(a != b); //true
        console.log(a >= b); //false
        console.log(a === 5); //true

   let f1 = "Orange",
        f2 = "orange";

    let result = f1 == f2;
        console.log(result); //false

    let result2 = f1.toLowerCase == f2.toLowerCase;
        console.log(result2); //true 

    console.log(null == undefined); //true
    console.log(null === undefined); //false
        

 3. Оператор присвоєння (=) призначає значення змінній.
    let a = 10; 
    console.log(a += 1); //11
    console.log(a -= 1); //10
    console.log(a *= 2); //20
    console.log(a /= 2); //10
    console.log(a %= 3); //1
    console.log(a &= 3); //1
    console.log(a |= 3); //3
    console.log(a <<= 1); //6
    console.log(a >>= 1); //3
    console.log(a >>>= 1); //1
*/

 //Практичні завдання:
 //1
    let username = "NotJane";
    console.log(username);

    let password = "notjanecooper";
    console.log(password);

    let userpassword = prompt ("Please enter your password");
    while(password !== userpassword) {
        console.log(password === userpassword);
        userpassword = prompt ("Please enter the correct password!");
    }
    console.log(userpassword);

    //2
    let x = 5;
    let y = 3;
    alert (x + y);
    alert (x - y);
    alert (x * y);
    alert (x / y);
    alert (x % y);
